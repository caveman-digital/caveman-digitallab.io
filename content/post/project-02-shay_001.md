---
title: "Project 02, Shay Class C locomotive"
date: 2017-12-01
thumbnailImagePosition: left
thumbnailImage: /img/shay-class-c-01/1280px-150-Ton-Shay.jpg
gallery:
- /../../../img/shay-class-c-01/boiler_print_001.jpg /../../../img/shay-class-c-01/boiler_print_001.jpg ""
categories:
- sawmill
- locomotives 
tags:
- sawmill
- kit
- projects
- locomotives
- shay
---

As I started work on the sawmill I also started looking at logging railroads and one iconic locomotive used on lots of the north american railroads at the beginning of the 1900's was the Shay locomotive. A gear driven locomotive that allowed it to be more adapted to the often narrow, steep and short turn railroad tracks around a logging camp.

Work has started on this locomotive and below are some photos of the first 3d prints. I'll be building this locomotive in HO scale and will trust again in Shapeways to get it printed but at the same time I am doing test prints and fits in O scale as that is something my FlashForge printer can handle and gives me good results. The image below was printed on my printer at home in O scale.

One of the tricky bits is to stay as close as possible to reality when it comes to sizing but I have to keep the dimensions and limits of the shapeways materials in Mind. I really like the look of their black high detail acrylic which has a minimum wall thickness of 0.6mm which is tiny but in HO scale that is a pretty common dimension so certain parts might have to be printed in the Frosted Extreme detail which can go as low as 0.3mm.

Below is a timelapse video of the creation of the boiler. Some parts are missing as some of the data got lost and at times I forgot to turn on the camera while modeling. I now have a better system and hopefully won't be loosing any data again.

{{< youtube V04jfMhfiAc >}}

I hope you enjoy this series and as always I am open to hear suggestions or comments.

Jozef
Caveman Digital.
