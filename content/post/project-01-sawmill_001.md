---
title: "Project 01, Sawmill"
date: 2017-09-01
thumbnailImagePosition: left
thumbnailImage: /img/timber.jpg
categories:
- sawmill
tags:
- sawmill
- kit
- projects
---

Because no railway is complete without a sawmill the first project I'll be working on is a sawmill. I will be working on this along side a Shay locomotive. I usually like having 2 projects on the go so that I can alternate and switch between them as often inspiration for one project comes while working on a different project.

The idea for this project is to design and build a sawmill that I can then also provide as a kit to other people that would be interested in building it. I'll be designing it in Fusion 360 and parts of it will be 3d printed (all the parts and details that are not wood will be 3d printed) and the rest will be build out of wood.
I'll be documenting the whole process and sharing it as I go with videos, blog posts and images.

I am really excited to get this project going. Over the past few weeks I have been gathering over 700 reference images of steam powered saw mills as well as looked at many videos and read many posts online on how these mills operate.
Tomorrow I'll be starting on  designing the log carriage and will be recording my process as I go through this.

Looking forward to sharing this!


Jozef
Caveman Digital.
