---
title: "Welcome to Caveman Digital"
thumbnailImagePosition: "top"
thumbnailImage: img/rsz_autumn-2480532_640.jpg
coverImage:  ../../../img/autumn-2480532_1280.jpg 
metaAlignment: center
coverMeta: out
date: 2017-08-06
weight: 100
categories:
- intro
tags:
- welcome
---
Caveman Digital is the result of a passion for technology, 3d modeling & printing and railway modeling.

<!--more-->
The passion for making is something I have had since I was little and something that was passed on to me by my parents and
the focus of Caveman Digital is to provide a technical channel where we go through projects from start to finish using todays technology.


The final projects will be railway related for the majority of them and will be projects that will take the various passions above to bring to completion so posts and videos
might be applicable to more than just that specific interest of mine.

A few things you can expect are 3d modeling walkthroughs using Fusion360 mostly, preparing models for 3D printing,  3D printing technics, assembly of models, painting, wiring and programming of some of the projects.

All in all it is about sharing what I enjoy and have learned and what I will learn from taking on some more focused and bigger projects and sharing that knowledge with the various communities.

Thank you and we hope you'll enjoy the content we'll be sharing.

Jozef van Eenbergen,
Caveman Digital.