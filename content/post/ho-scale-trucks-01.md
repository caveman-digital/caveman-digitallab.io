---
title: "100t HO scale trucks first prints"
thumbnailImagePosition: "left"
thumbnailImage: /img/ho-scale-trucks-02/ho_truck.jpg 
metaAlignment: center
coverMeta: out
date: 2017-08-09
categories:
- truck
tags:
- HO
- truck
---
Yesterday I received the first set of HO scale trucks that I modelled using Fusion360 and got printed at Shapeways.

<!--more-->

{{< instagram BXlGgQSDdPG hidecaption >}}

I have used various 3D packages throughout my career and only recently picked up Fusion360 after I got my first 3d printer.
At first it was a bit tricky to get my head around being used to certain things in other packages but the speed and ease with which things can be done is amazing.

This is the first succesful print of these trucks but I probably started over 3 times. Getting to know the limitations with Shapeways and their FUD material I had to start 
over a couple of times just to get to a model that was easy to tweak and adapt depending on what I got as feedback from the Shapeways analyses.

Sticking to all parameters however means that you get what you model and I am truely amazed by the amount of detail they managed to capture in this print. I had printed this on my home
printer which is a simple FDM printer so impossible to get details not bigger than 0.3mm to show up properly where for Shapeways this was not a problem.

So why did I decide to create trucks? There are plenty available on the market for very competitive prices?.
The goal for me with these trucks was to see what was possible in regards to detail. I know what can be done in regards to detail when simply working on a computer and that
is something that is slightly addictive. The more details, the closer you can get something to reality to more fun I have making these models. So when I picked up a railway starter kit
to get me going with my own layout I was a bit disappointed by the lack of detail when I compared them to real trucks. I started looking around and I only found some Kato models that
had a higher amount of detail but they were expensive compared to what I could make myself and get printed and modeling them myself meant I could include all the details I wanted and use it
as a good start to get my head around Fusion360 before tackling a bigger project.

Finding references for trucks however is not the easiest. It took me quite some time to get some good images of the trucks I was after. One particularly useful video was:

{{< youtube VMReh0VJ-2Q >}}


I am pretty happy with how these turned out, there are a few minor tweaks left. The space between the two bolistors is a little bit too narrow the wheels fit but getting them 
into the trucks is too thight and with the FUD being not as flexible as standard plastic there is a chance of breaking them so I'll make things a bit wider.
At the moment I print the bearings in the actual truck but there is a tolerance there that should allow them to rotate but this is a 0.07mm tolerance which is very minimal.
Instead I'll print these outside of the trucks. This means there is a bit more space for the axles themselves to fit into the bearing hole and it will allow the bearings to be attached 
to the axles and with a bit of luck spin when the wheels are turning.


Jozef
Caveman Digital.
