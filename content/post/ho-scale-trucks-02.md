---
title: "100t HO scale trucks adjustments"
date: 2017-08-09
thumbnailImagePosition: left
thumbnailImage: /img/ho-scale-trucks-02/cn_ho_trucks_01.jpg
gallery:
- /../../../img/ho-scale-trucks-02/cn_ho_trucks_01.jpg /../../../img/ho-scale-trucks-02/cn_ho_trucks_01.jpg ""
- /../../../img/ho-scale-trucks-02/cn_ho_trucks_02.jpg /../../../img/ho-scale-trucks-02/cn_ho_trucks_02.jpg ""
- /../../../img/ho-scale-trucks-02/cn_ho_trucks_03.jpg /../../../img/ho-scale-trucks-02/cn_ho_trucks_03.jpg ""
- /../../../img/ho-scale-trucks-02/100t_truck_assembly.png
- /../../../img/ho-scale-trucks-02/100t_truck_dimensions.png 
categories:
- truck
tags:
- HO
- truck
---

With the first batch of trucks in, I spent some time this weekend on replacing the existing trucks on this CN rail car.
This meant getting the right screws, some metal wheels and making sure everything fitted.

The main issue I ran into is that although there was enough space for the wheels, getting the needle pins from the wheels in was requiring more force than I'd like especially with the FUD materials from shapeways being a bit less resistant to bending. I got them in but that also got me thinking on how to avoid having to bend them. More about that later.

I tried various types of screws that would fit through the hole at the bottom with the screw head so they'd be invisible. I found the best screws for this are 2-56. I had some of those laying around from Woodland scenics. Having a look further online it looks like these 2-56's can be bought by the hundred for about $5.


To avoid the bending I went back into Fusion360 and changed a few things. The assembly can be seen in the gallery below. There are now 3 parts to the truck. The main part of the truck which will hold everything together. An axle/bearing socket where the bearing goes into and the bearing itself. The idea being that we slide the wheel into the axle bearing socket when then slide the bearing overtop of the needle pin from the wheel to keep it locked in place. We do this on both sides and then lower down the truck itself onto the bearing socket for which slots have been made into the truck. This will allow for the wheels to be already mounted and will not require the truck itself to be bend.
An added bonus is that the bearings themselves should now properly rotate with the wheels as well where before, eventhough there was enough clearance, these ended up 'melted' together for the batch of trucks I got last week.

I have ordered a new batch of the trucks from shapeways hopefully they'll be in this week.




Jozef
Caveman Digital.
